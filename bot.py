#!/usr/bin/env python3

import telegram
from telegram import ChatAction
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import random
import os
from graf import Graf

TOKEN = "789966531:AAGpTTIrALFBXkLewdQr3OMBYHF3rKD9kuo"


def catch_exceptions(f):
    def g(bot, update, *args, **kwargs):
        try:
            f(bot, update, *args, **kwargs)
        except:
            bot.send_message(chat_id=update.message.chat_id, text="💣 S'ha produït un error inesperat. Contacta amb els desenvolupadors.")
            author(bot, update)
    return g


@catch_exceptions
def start(bot, update, chat_data):
    '''Initializes a graph for the user'''
    bot.send_message(chat_id=update.message.chat_id, parse_mode=telegram.ParseMode.MARKDOWN, text="*Benvingut al GraphBot!*")
    bot.send_message(chat_id=update.message.chat_id, text="Escriu /help si necessites ajuda")
    chat_data["graf"] = default_graph


@catch_exceptions
def help_(bot, update):
    '''Sends help message'''
    try:
        mess = """Instruccions:
    • /start: Comença la conversa amb el GraphBot
    • /help: Mostra aquest missatge d'ajuda.
    • /graph ⟨distància⟩ ⟨població⟩: Crea un graf amb les ciutats de més de ⟨distància⟩ habitants separades per menys de ⟨distància⟩ km.
    • /nodes: Compta els nodes del graf
    • /edges: Compta les arestes del graf
    • /components: Compta les components connexes del graf
    • /plotpop ⟨dist⟩ \[⟨lat⟩ ⟨lon⟩]: Mostra un mapa amb totes les ciutats del graf a distància menor o igual que ⟨dist⟩. La mida dels marcadors és proporcional a la població. Opcionalment pots especificar unes coordenades diferents a les teves.
    • /plotgraph ⟨dist⟩ \[⟨lat⟩ ⟨lon⟩]: Mostra un mapa amb totes les ciutats del graf a distància menor o igual que ⟨dist⟩ i les arestes que les uneixen. Opcionalment pots especificar unes coordenades diferents a les teves.
    • /route ⟨src⟩ ⟨dest⟩: Mostra el camí més curt entre ⟨src⟩ i (dest). Els paràmetres han d'estar en el format "Barcelona, es", amb cometes.
    • /author: Mostra com contactar amb l'autor del bot.
    """
        bot.send_message(chat_id=update.message.chat_id, parse_mode=telegram.ParseMode.MARKDOWN, text=mess)
    except Exception as e:
        print(e)
        bot.send_message(chat_id=update.message.chat_id, text='💣')


def author(bot, update):
    '''Sends the author's contact info'''
    bot.send_message(chat_id=update.message.chat_id, parse_mode=telegram.ParseMode.MARKDOWN, text="Ferran López Guitart\n```ferran.lopez.guitart@est.fib.upc.edu```")


@catch_exceptions
def graph(bot, update, args, chat_data):
    '''Generates a new graph for the user with min population args[0] and max distance args[1]'''
    try:
        try:
            dist = float(args[0])
            pop = float(args[1])
            fct = pop/dist
            if pop < 100000:
                bot.send_message(chat_id=update.message.chat_id, text="El graf és massa gran. Augmenta la població mínima.")
                return
            if fct < 200:
                bot.send_message(chat_id=update.message.chat_id, text="El graf és massa gran. Augmenta la població mínima o redueix la distància.")
                return
        except:
            bot.send_message(chat_id=update.message.chat_id, text="Els arguments han de ser enters positius.")
            return
        bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        chat_data["graf"] = Graf(float(args[0]), float(args[1]))
        bot.send_message(chat_id=update.message.chat_id, text=f"S'ha creat un nou graf amb les ciutats de més de {args[1]} habitants separades per menys de {args[0]} km.")
    except Exception as e:
        print(e)
        unexpected_error()


@catch_exceptions
def nodes(bot, update, chat_data):
    '''Sends the number of nodes of the user's graph.'''
    print(chat_data["graf"])
    n = chat_data["graf"].number_of_nodes()
    bot.send_message(chat_id=update.message.chat_id, text=f"{n}")


@catch_exceptions
def edges(bot, update, chat_data):
    '''Sends the number of edges of the user's graph.'''
    n = chat_data["graf"].number_of_edges()
    bot.send_message(chat_id=update.message.chat_id, text=f"{n}")


@catch_exceptions
def components(bot, update, chat_data):
    '''Sends the number of connected components of the user's graph.'''
    n = chat_data["graf"].number_of_components()
    bot.send_message(chat_id=update.message.chat_id, text=f"{n}")


@catch_exceptions
def plotpop(bot, update, args, chat_data):
    '''Sends a population map of the cities with distance less than args[0] relative to the coordinates (args[1], args[2]), or to the user's location, if no further args are specified.'''
    try:
        bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        if len(args) > 3 or len(args) == 2:
            bot.send_message(chat_id=update.message.chat_id, text="El nombre d'arguments no és correcte.")
            return
        if len(args) < 1:
            bot.send_message(chat_id=update.message.chat_id, text="Has d'especificar una distància.")
            return
        if len(args) < 2:
            if "location" not in chat_data:
                bot.send_message(chat_id=update.message.chat_id, text="Necessitem que ens enviis la teva ubicació o especifiquis unes coordenades per saber les ciutats que tens a prop. 😅")
                return
            coords = chat_data["location"]
        else:
            coords = (float(args[1]), float(args[2]))
        img = chat_data["graf"].plotpop(float(args[0]), coords)
        if img is None:
            bot.send_message(chat_id=update.message.chat_id, text="No s'ha trobat cap ciutat")
            return
        fname = f"{random.randint(1000000, 9999999)}.png"
        img.save(fname)
        bot.send_photo(chat_id=update.message.chat_id, photo=open(fname, 'rb'))
        os.remove(fname)
    except Exception as e:
        print(e)
        bot.send_message(chat_id=update.message.chat_id, text='Si us plau, comprova que el format dels arguments és correcte.')


@catch_exceptions
def plotgraph(bot, update, args, chat_data):
    '''Sends a map of the cities, and edges between cities, with distance less than args[0] relative to the coordinates (args[1], args[2]), or to the user's location, if no further args are specified.'''
    try:
        bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        if len(args) > 3 or len(args) == 2:
            bot.send_message(chat_id=update.message.chat_id, text="El nombre d'arguments no és correcte.")
            return
        if len(args) < 1:
            bot.send_message(chat_id=update.message.chat_id, text="Has d'especificar una distància.")
            return
        if len(args) < 2:
            if "location" not in chat_data:
                bot.send_message(chat_id=update.message.chat_id, text="Necessitem que ens enviis la teva ubicació o especifiquis unes coordenades per saber les ciutats que tens a prop. 😅")
                return
            coords = chat_data["location"]
        else:
            coords = (float(args[1]), float(args[2]))
        img = chat_data["graf"].plotgraph(float(args[0]), coords)
        if img is None:
            bot.send_message(chat_id=update.message.chat_id, text="No s'ha trobat cap ciutat")
            return
        fname = f"{random.randint(1000000, 9999999)}.png"
        img.save(fname)
        bot.send_photo(chat_id=update.message.chat_id, photo=open(fname, 'rb'))
        os.remove(fname)
    except Exception as e:
        print(e)
        bot.send_message(chat_id=update.message.chat_id, text='Si us plau, comprova que el format dels arguments és correcte.')
        

@catch_exceptions
def route(bot, update, args, chat_data):
    '''Sends a map of the route between cities args[0] and args[1].'''
    try:
        bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        a = [s.replace(" ", "").replace('"', "").lower() for s in "".join(args).split('"') if s != ""]
        img = chat_data["graf"].plotroute(a[0], a[1])
        if img[1] != 0:
            if img[1] == 1:
                bot.send_message(chat_id=update.message.chat_id, text="La ciutat d'origen no existeix.")
                return
            if img[1] == 2:
                bot.send_message(chat_id=update.message.chat_id, text="La ciutat de destí no existeix.")
                return
            if img[1] == 3:
                bot.send_message(chat_id=update.message.chat_id, text="No hi ha cap ruta entre les dues ciutats.")
                return
        fname = f"{random.randint(1000000, 9999999)}.png"
        img[0].save(fname)
        bot.send_photo(chat_id=update.message.chat_id, photo=open(fname, 'rb'))
        os.remove(fname)
    except Exception as e:
        print(e)
        bot.send_message(chat_id=update.message.chat_id, text='Si us plau, comprova que el format dels arguments és correcte.')


@catch_exceptions
def where(bot, update, chat_data):
    '''Saves the user's location'''
    lat, lon = update.message.location.latitude, update.message.location.longitude
    chat_data["location"] = (lat, lon)
    bot.send_message(chat_id=update.message.chat_id, text='Ubicació rebuda')


if __name__ == "__main__":

    updater = Updater(token=TOKEN)
    dispatcher = updater.dispatcher

    # Command and message handlers
    dispatcher.add_handler(CommandHandler('start', start, pass_chat_data=True))
    dispatcher.add_handler(CommandHandler('help', help_))
    dispatcher.add_handler(CommandHandler('author', author))
    dispatcher.add_handler(CommandHandler('graph', graph, pass_args=True, pass_chat_data=True))
    dispatcher.add_handler(CommandHandler('nodes', nodes, pass_chat_data=True))
    dispatcher.add_handler(CommandHandler('edges', edges, pass_chat_data=True))
    dispatcher.add_handler(CommandHandler('components', components, pass_chat_data=True))
    dispatcher.add_handler(CommandHandler('plotpop', plotpop, pass_args=True, pass_chat_data=True))
    dispatcher.add_handler(CommandHandler('plotgraph', plotgraph, pass_args=True, pass_chat_data=True))
    dispatcher.add_handler(CommandHandler('route', route, pass_args=True, pass_chat_data=True))
    dispatcher.add_handler(MessageHandler(Filters.location, where, pass_chat_data=True))

    # Generate default graph for new users. This improves performace later.
    default_graph = Graf(300, 100000)
    print("Ready!")
    
    # Start the bot
    updater.start_polling()
