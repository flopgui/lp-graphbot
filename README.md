# Graphbot

Telegram bot which generates a graph of cities and answers questions about the graph. It can generate new graphs with parameters given by the user. It's also capable of calculating numerical data about the graph, plotting cities near the user on a map and drawing routes between cities.

This bot is a project for the [Programming Languages](https://www.fib.upc.edu/en/studies/bachelors-degrees/bachelor-degree-informatics-engineering/curriculum/syllabus/LP) course at FIB, Polytechnic University of Catalonia.

## Getting Started

### Prerequisites

This project requires python and pip to be installed on your machine. To install the required python libraries, run this command from the project folder:

```
pip install -r requirements.txt
```

### Installing

To run the bot, just run

```
python3 bot.py
```

And wait until the program tells you it's ready. The first time you run the program this may take a while, because it needs to download the dataset.

### Using the bot

Open Telegram and open a chat with the user @ferran_graphbot. Send the command

```
/start
```

If you need help, send `/help` to the bot.

## Running the tests

### Test run

This is a sample run from the bot. First of all, run the start command

```
/start
```

This will generate a default map with cities with population above 100000 and edges if the distance between them is less than 300 km. Now run the following commands:

```
/nodes
/edges
/components
```

Which gives the number of nodes, edges and components of the graph. The results should be  3527, 48086 and 163. Now, if you run

```
/graph 600 500000
```

the bot will create a new graph with cities above half a million inhabitants and edges if the distance is less than 600 km. Check that the graph now has less nodes and edges. The bot can also generate maps, for example with

```
/plotpop 1000 41.3888 2.1590
```

you'll get a map of the cities less than 1000 km away from Barcelona, with sizes proportional to population. You can now send your current location (with the paperclip icon next to the writing prompt) and re-run the command only with the first parameter, the bot will send a map with cities less than the specified distance from you.

If you replace `plotpop` with `plotmap`, it will also draw edges between nearby cities. Now you can send this command:

```
/route "varselona, es", "pariss, fr"
```

The answer will be a map with the route between the two cities. Note the bot could interpret the command even though the names had typos. If you try to find a non-existent route, for example between "barcelona, es" and "tokyo, jp", the bot wil tell you it doesn't exist.

If you try with another user or device, you'll notice the bot remembers which chat is in and uses the correct graph. If you want to get more information about these and other commands, send `/help` to the bot.

### Coding style tests

Go to [pep8online](http://pep8online.com/) and paste the contents of each *.py file to check their compliance with pep8.


## Author

Ferran López
[ferran.lopez.guitart@est.fib.upc.edu]

