import requests
import pandas
import networkx
import os
from fuzzywuzzy import fuzz
from haversine import haversine
from staticmap import StaticMap, CircleMarker, Line
from quadtree import QuadTree

DATA_URL = "https://github.com/jordi-petit/lp-graphbot-2019/blob/master/dades/worldcitiespop.csv.gz"
DATA_URL_PARAMS = {"raw": "true"}
DATA_FILENAME = "worldcitiespop.csv.gz"
MAP_SIZE = (600, 600)


def download():
    '''Downloads data file from DATA_URL and saves it in DATA_FILENAME'''
    r = requests.get(DATA_URL, params=DATA_URL_PARAMS, stream=True)
    with open(DATA_FILENAME, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
                f.flush()


class Graf():
    '''Represents a graph whose nodes are cities and where two cities are linked iff the distance between them is below a certain treshold. It also saves information about the country, name, population and location of each city'''
    
    def __init__(self, distance, population):
        if not os.path.isfile(DATA_FILENAME):
            print("Downloading data file (this may take a while)...")
            download()
        self.cities = pandas.read_csv(DATA_FILENAME)
        self.cities = self.cities[self.cities["Population"] > population]
        self.build_graph(distance)
        
    def get_cities(self, filename, minpop):
        '''Reads the list of cities'''
        f = pandas.read_csv(filename)
        i = 0
        for _, n in nodes.iterrows():
            if n["Population"] == "" or float(n["Population"]) < minpop:
                continue
            pop = float(n["Population"])
            data = {"country": n["Country"], "city": n["City"], "accentcity": n["AccentCity"], "population": pop}
            coords1 = (float(n["Latitude"]), float(n["Longitude"]))
            self.cities.append((data, coords1))
            i += 1
        
    def build_graph(self, distance):
        '''Builds a graph with the cities of self.cities as nodes and connecting cities less than distance km away'''
        self.gr = networkx.Graph()
        self.qt = QuadTree(90, -90, -180, 180)
        for i, n in self.cities.iterrows():
            coords = (n["Latitude"], n["Longitude"])
            self.gr.add_node(i)
            nn = self.qt.get_nearby_nodes(coords, distance)
            for j in nn:
                d = haversine(coords, j[1])
                self.gr.add_edge(i, j[0], dist=d)
            self.qt.add_node(i, coords)
            
    def number_of_nodes(self):
        '''Returns the number of nodes of the graph.'''
        return self.gr.number_of_nodes()
    
    def number_of_edges(self):
        '''Returns the number of edges of the graph.'''
        return self.gr.number_of_edges()
    
    def number_of_components(self):
        '''Returns the number of connected components of the graph.'''
        return networkx.number_connected_components(self.gr)
    
    def shortest_path(self, origin, destination):
        '''Returns the shortest path between origin and destination.'''
        try:
            return networkx.shortest_path(self.gr, origin, destination, weight="dist")
        except networkx.NetworkXNoPath:
            return None
    
    def nearby_cities(self, dist, coord):
        '''Returns the subgraph of gr with the nodes corresponding to cities less than dist km away from coord.'''
        nc = self.qt.get_nearby_nodes(coord, dist)
        return self.gr.subgraph([t[0] for t in nc])
    
    def plotpop(self, dist, coord):
        '''Returns an image of a map with the cities less than dist km away from coord, ith marker size proportional to population.'''
        gr = self.nearby_cities(dist, coord)
        mapa = StaticMap(*MAP_SIZE)
        nodes = self.cities.loc[list(gr.nodes())]
        if len(nodes) == 0:
            return None
        maxpop = max(nodes["Population"])
        for _, n in nodes.iterrows():
            size = 30 * n["Population"] / maxpop
            marker = CircleMarker((n["Longitude"], n["Latitude"]), "red", size)
            mapa.add_marker(marker)
        image = mapa.render()
        return image
    
    def plotgraph(self, dist, coord):
        '''Returns an image of a map with the cities less than dist km away from coord and the edges between them.'''
        gr = self.nearby_cities(dist, coord)
        mapa = StaticMap(*MAP_SIZE)
        nodes = self.cities.loc[list(gr.nodes())]
        if len(nodes) == 0:
            return None
        maxpop = max(nodes["Population"])
        for _, n in nodes.iterrows():
            marker = CircleMarker((n["Longitude"], n["Latitude"]), "red", 8)
            mapa.add_marker(marker)
        for a in gr.edges():
            n1 = nodes.loc[a[0]]
            n2 = nodes.loc[a[1]]
            line = Line(((n1["Longitude"], n1["Latitude"]), (n2["Longitude"], n2["Latitude"])), "blue", 2)
            mapa.add_line(line)
        image = mapa.render()
        return image
        
    def routeplot(self, route):
        '''Returns an image of a map with route marked.'''
        rroute = self.cities.loc[route]
        first = True
        mapa = StaticMap(*MAP_SIZE)
        for _, n2 in rroute.iterrows():
            marker = CircleMarker((n2["Longitude"], n2["Latitude"]), "red", 5)
            mapa.add_marker(marker)
            if first:
                first = False
            else:
                line = Line(((n1["Longitude"], n1["Latitude"]), (n2["Longitude"], n2["Latitude"])), "blue", 1)
                mapa.add_line(line)
            n1 = n2
        image = mapa.render()
        return image
    
    def find_city(self, name):
        '''Finds the city with the most similar name to name. If none exists with a similar enoug name, returns None.'''
        c = [s.strip().lower() for s in name.split(",")]
        data = self.cities[self.cities["Country"] == c[1]]
        sim = 100
        d = data[[fuzz.ratio(city.lower().replace(" ", ""), c[0]) >= sim for city in data["AccentCity"]]]
        while sim >= 75 and len(d) == 0:
            sim -= 5
            d = data[[fuzz.ratio(city.lower().replace(" ", ""), c[0]) >= sim for city in data["AccentCity"]]]
        if len(d) == 0:
            return None
        return d.iloc[0]
    
    def plotroute(self, origin, destination):
        '''Returns a map with the route between origin and destination and the. If either origin or destination doesn't exist, or if there isn't a path between both, returns None and an error code (1, 2 or 3 respectively).'''
        o = self.find_city(origin)
        d = self.find_city(destination)
        if o is None:
            return (None, 1)
        if d is None:
            return (None, 2)
        r = self.shortest_path(o.name, d.name)
        if r is None:
            return (None, 3)
        return (self.routeplot(r), 0)
