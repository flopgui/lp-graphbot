from haversine import haversine
from random import random


class QuadTree():
    '''Tree structure where each node has exactly four children.
    Its purpose is to store a set of points on the world's surface
    and be able to efficiently find the closest neighbors of a given point.
    An instance is meant to represent a rectangular portion of the world's surface.
    Each of the four children cover a quadrant of that portion (NW, NE, SW, SE).
    A child can be empty, contain a point or be further subdivided into four quadrants.
    '''
    
    def __init__(self, nlim, slim, wlim, elim):
        self.nlim = nlim
        self.slim = slim
        self.elim = elim
        self.wlim = wlim
        self.children = {"NW": None, "NE": None, "SW": None, "SE": None}

    def add_node(self, data, coords):
        '''Adds a node to the tree.'''
        latm = (self.nlim + self.slim) / 2  # Limits between the quadrants
        lonm = (self.wlim + self.elim) / 2
        if coords[0] > latm:
            if coords[1] < lonm:
                q = "NW"
            else:
                q = "NE"
        else:
            if coords[1] < lonm:
                q = "SW"
            else:
                q = "SE"
        if coords[0] == latm:
            q = ("N" if random() > 0.5 else "S") + q[1]
        if coords[1] == lonm:
            q = q[0] + ("E" if random() > 0.5 else "W")
        if self.children[q] is None:
            self.children[q] = (data, coords)
        elif isinstance(self.children[q], QuadTree):
            self.children[q].add_node(data, coords)
        else:
            old = self.children[q]
            if old[1] == coords:
                return
            if q == "NW":
                limits = [self.nlim, latm, self.wlim, lonm]
            elif q == "NE":
                limits = [self.nlim, latm, lonm, self.elim]
            elif q == "SW":
                limits = [latm, self.slim, self.wlim, lonm]
            else:
                limits = [latm, self.slim, lonm, self.elim]
            self.children[q] = QuadTree(*limits)
            self.children[q].add_node(*old)
            self.children[q].add_node(data, coords)
    
    def includes(self, coords):
        '''Returns true if coords are in the rectangle represented by the QuadTree.'''
        return self.nlim >= coords[0] >= self.slim and self.wlim <= coords[1] <= self.elim

    def reachable(self, coords, dist):
        '''Returns true if coords are inside or less than dist km away
        from the rectangle represented by the QuadTree.'''
        if self.includes(coords):
            return True
        if haversine(coords, (coords[0], self.elim)) <= dist:
            return True
        if haversine(coords, (coords[0], self.wlim)) <= dist:
            return True
        if haversine(coords, (self.nlim, coords[1])) <= dist:
            return True
        if haversine(coords, (self.slim, coords[1])) <= dist:
            return True
        return False
        
    def get_nearby_nodes(self, coords, dist):
        '''Returns a list of the nodes whose distance to coords is less then dist km.'''
        nn = []
        for _, t in self.children.items():
            if t is None:
                continue
            elif isinstance(t, QuadTree):
                if t.reachable(coords, dist):
                    nn += t.get_nearby_nodes(coords, dist)
            else:
                if haversine(coords, t[1]) < dist:
                    nn.append(t)
        return nn
